package com.morozov.tm.command;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserRegistryCommand extends AbstractCommand {
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    final public String getName() {
        return "user-reg";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Registry new user";
    }

    @Override
    final public void execute() throws UserExistException_Exception, StringEmptyException_Exception {
        ConsoleHelperUtil.writeString("Введите имя пользователя");
        @NotNull final String login = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите пароль пользователя");
        @NotNull final String password = ConsoleHelperUtil.readString();
        if(login.isEmpty() || password.isEmpty()) throw  new StringEmptyException_Exception();
        userEndpoint.registryUser(login, password);
        ConsoleHelperUtil.writeString("Пользователь с логином " + login + " зарегистрирован");
    }
}
