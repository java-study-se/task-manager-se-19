package com.morozov.tm.command.project;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.CompareTypeUnum;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ComparatorFactoryUtil;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class ProjectListCommand extends AbstractCommand {
    @Autowired
    private ProjectEndpoint projectEndpoint;
    @Autowired
    private IServiceLocator serviceLocator;

    public ProjectListCommand() {
        userRoleList.add(UserRoleEnum.USER);
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @NotNull
    @Override
    final public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Show all projects";
    }

    @Override
    final public void execute() throws AccessFirbidenException_Exception, CloneNotSupportedException_Exception,
            UserNotFoundException_Exception {
        final @Nullable SessionDto session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        @NotNull final List<ProjectDto> projectList = projectEndpoint.findAllProjectByUserId(session);
        if (projectList.isEmpty()) {
            ConsoleHelperUtil.writeString("Список проектов пуст");
            return;
        }
        ConsoleHelperUtil.writeString("Список проектов пользователя :");
        @NotNull final CompareTypeUnum compareTypeUnum = ConsoleHelperUtil.printSortedVariant();
        if (compareTypeUnum != null) {
            ConsoleHelperUtil.writeString("Сортировка по " + compareTypeUnum.getName());
            @NotNull final ProjectDto[] projects = projectList.toArray(new ProjectDto[0]);
            Arrays.sort(projects, ComparatorFactoryUtil.getComparator(compareTypeUnum));
            ConsoleHelperUtil.printProjectList(Arrays.asList(projects));
        } else {
            ConsoleHelperUtil.writeString("Сортировка не выбрана.");
            ConsoleHelperUtil.printProjectList(projectList);
        }
    }
}

