package com.morozov.tm.command;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserShowCommand extends AbstractCommand {
    @Autowired
    private IServiceLocator serviceLocator;
    @Autowired
    private UserEndpoint userEndpoint;

    public UserShowCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "user-show";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Show current user";
    }

    @Override
    final public void execute() throws AccessFirbidenException_Exception, UserNotFoundException_Exception,
            CloneNotSupportedException_Exception {
        final @Nullable SessionDto session = serviceLocator.getSession();
        final UserDto currentUser = userEndpoint.findOneUserById(session);
        if (currentUser != null) {
            ConsoleHelperUtil.writeString("Имя текущего пользователя: " + currentUser.getLogin());
            ConsoleHelperUtil.writeString("ID текущего пользователя: " + currentUser.getId());
            ConsoleHelperUtil.writeString("Права текущего пользователя: " + currentUser.getRole().value());
        } else {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
        }
    }
}
