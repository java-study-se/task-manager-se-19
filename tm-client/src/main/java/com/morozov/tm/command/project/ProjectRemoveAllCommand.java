package com.morozov.tm.command.project;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProjectRemoveAllCommand extends AbstractCommand {
    @Autowired
    private ProjectEndpoint projectEndpoint;
    @Autowired
    private IServiceLocator serviceLocator;

    public ProjectRemoveAllCommand() {
        userRoleList.add(UserRoleEnum.USER);
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "project-remove-all";
    }

    @Override
    public String getDescription() {
        return "Remove all project current user";
    }

    @Override
    public void execute() throws CloneNotSupportedException_Exception, AccessFirbidenException_Exception,
            ConnectionLostException_Exception, SqlCustomException_Exception, UserNotFoundException_Exception {
        final @Nullable SessionDto session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        ConsoleHelperUtil.writeString("Удаление задач пользователя");
        ConsoleHelperUtil.writeString("Удаление проектов пользователя ");
        projectEndpoint.removeAllProjectByUserId(session);
        ConsoleHelperUtil.writeString("Удаление завершено");
    }
}

