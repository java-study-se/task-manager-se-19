package com.morozov.tm.command.project;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProjectRemoveCommand extends AbstractCommand {
    @Autowired
    private ProjectEndpoint projectEndpoint;
    @Autowired
    private IServiceLocator serviceLocator;

    public ProjectRemoveCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Remove selected project";
    }

    @Override
    final public void execute() throws AccessFirbidenException_Exception, StringEmptyException_Exception,
            CloneNotSupportedException_Exception, UserNotFoundException_Exception, ProjectNotFoundException_Exception {
        final @Nullable SessionDto session = serviceLocator.getSession();
        ConsoleHelperUtil.writeString("Введите ID проекта для удаления");
        @NotNull final String idDeletedProject = ConsoleHelperUtil.readString();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        if (projectEndpoint.removeProjectById(session, idDeletedProject)) {
            ConsoleHelperUtil.writeString("Проект с ID: " + idDeletedProject + " удален");
        } else {
            ConsoleHelperUtil.writeString("Проекта с данным ID не существует");
        }
    }
}
