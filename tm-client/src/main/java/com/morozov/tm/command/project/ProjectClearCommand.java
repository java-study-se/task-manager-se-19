package com.morozov.tm.command.project;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProjectClearCommand extends AbstractCommand {
    @Autowired
    private ProjectEndpoint projectEndpoint;
    @Autowired
    private IServiceLocator serviceLocator;
    @Autowired
    private TaskEndpoint taskEndpoint;

    public ProjectClearCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @NotNull
    @Override
    final public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Remove all projects";
    }

    @Override
    final public void execute() throws CloneNotSupportedException_Exception, AccessFirbidenException_Exception,
            UserNotFoundException_Exception {
        final @Nullable SessionDto session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        taskEndpoint.clearTaskList(session);
        ConsoleHelperUtil.writeString("Список задач очищен");
        projectEndpoint.clearProjectList(session);
        ConsoleHelperUtil.writeString("Список проектов очищен");
    }
}
