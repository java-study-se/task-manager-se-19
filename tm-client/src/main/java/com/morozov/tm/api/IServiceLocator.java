package com.morozov.tm.api;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IServiceLocator {
    List<AbstractCommand> getCommandList();

    @Nullable SessionDto getSession();

    void setSession(@Nullable SessionDto session);
}
