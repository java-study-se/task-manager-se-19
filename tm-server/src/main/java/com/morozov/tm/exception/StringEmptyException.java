package com.morozov.tm.exception;

public class StringEmptyException extends Exception {
    public StringEmptyException() {
        super("Введенные поля не могут быть пустими");
    }
}
