package com.morozov.tm.endpoint;

import com.morozov.tm.api.service.IDomainService;
import com.morozov.tm.api.service.ISessionService;
import com.morozov.tm.api.service.IUserService;
import com.morozov.tm.dto.DomainDto;
import com.morozov.tm.dto.SessionDto;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.AccessFirbidenException;
import com.morozov.tm.exception.UserNotFoundException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
@Component
public class DomainEndpoint {
    @Autowired
    private ISessionService sessionService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IDomainService domainService;


    @WebMethod
    public void load(
            @NotNull @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "domain") final DomainDto domainDto)
            throws CloneNotSupportedException, AccessFirbidenException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        domainService.load(domainDto);

    }

    @WebMethod
    public void export(
            @NotNull @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "domain") final DomainDto domainDto)
            throws CloneNotSupportedException, AccessFirbidenException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        domainService.export(domainDto);
    }
}
