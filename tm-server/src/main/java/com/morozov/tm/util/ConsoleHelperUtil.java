package com.morozov.tm.util;

import com.morozov.tm.dto.ProjectDto;
import com.morozov.tm.dto.TaskDto;
import com.morozov.tm.enumerated.CompareTypeUnum;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ConsoleHelperUtil {

    @NotNull
    public static final String DATA_FORMAT = "dd.MM.yyyy";

    @NotNull
    public static String readString() {
        @NotNull String enterString = "";
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            enterString = bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return enterString;
    }

    public static void writeString(@NotNull final String string) {
        System.out.println(string);
    }

    public static Date formattedData(@NotNull final String data) throws ParseException {
        if (data.isEmpty()) return null;
        @NotNull final SimpleDateFormat format = new SimpleDateFormat(DATA_FORMAT);
        @Nullable final Date resultDate = format.parse(data);
        return resultDate;
    }

    public static void printProjectList(@NotNull final List<ProjectDto> projectDtoList) {
        int indexList = 1;
        for (final ProjectDto projectDto : projectDtoList) {
            final String projectName = projectDto.getName();
            final String dataCreate = formattedDataToString(projectDto.getCreatedData());
            final String dataStart = formattedDataToString(projectDto.getStartDate());
            final String dataEnd = formattedDataToString(projectDto.getEndDate());
            final String currentStatus = projectDto.getStatus().getDisplayName();
            final String id = projectDto.getId();
            ConsoleHelperUtil.writeString(String.format("%d: Имя : %s | Дата создания: %s |" +
                            "| дата начала: %s | дата завершения: %s | Статус: %s | ID: %s",
                    indexList, projectName, dataCreate, dataStart, dataEnd, currentStatus, id));
            indexList++;
        }
    }
    public static void printTaskList(@NotNull final List<TaskDto> projectList) {
        int indexList = 1;
        for (final TaskDto taskDto : projectList) {
            final String projectName = taskDto.getName();
            final String dataCreate = formattedDataToString(taskDto.getCreatedData());
            final String dataStart = formattedDataToString(taskDto.getStartDate());
            final String dataEnd = formattedDataToString(taskDto.getEndDate());
            final String currentStatus = taskDto.getStatus().getDisplayName();
            final String id = taskDto.getId();
            ConsoleHelperUtil.writeString(String.format("%d: Имя : %s | Дата создания: %s |" +
                            "| дата начала: %s | дата завершения: %s | Статус: %s | ID: %s",
                    indexList, projectName, dataCreate, dataStart, dataEnd, currentStatus, id));
            indexList++;
        }
    }

    public static String formattedDataToString(Date date) {
        if(date == null) return "не установлена";
        @NotNull final SimpleDateFormat dateFormat = new SimpleDateFormat(DATA_FORMAT);
        return dateFormat.format(date);
    }

    public static CompareTypeUnum printSortedVariant() {
        ConsoleHelperUtil.writeString("Варианты сортировки: ");
        final HashMap<String, CompareTypeUnum> compareTypeEnumHashMap = new HashMap<>();
        for (CompareTypeUnum compareTypeUnum : CompareTypeUnum.values()) {
            String enumName = compareTypeUnum.getName();
            System.out.println(enumName + " : " + compareTypeUnum.getDescription());
            compareTypeEnumHashMap.put(enumName, compareTypeUnum);
        }
        final String type = ConsoleHelperUtil.readString();
        return compareTypeEnumHashMap.get(type);
    }
}
