package com.morozov.tm.entity;

import com.morozov.tm.enumerated.UserRoleEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;


@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_session")
public class Session extends AbstractEntity implements Cloneable {
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;
    @Nullable
    private String signature;
    @Nullable
    private Date timestamp;

    @Override
    public Session clone() throws CloneNotSupportedException {
        return (Session) super.clone();
    }
}
