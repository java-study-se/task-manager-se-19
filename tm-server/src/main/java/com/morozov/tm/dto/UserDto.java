package com.morozov.tm.dto;

import com.morozov.tm.enumerated.UserRoleEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class UserDto extends AbstractEntityDto {
    @NotNull
    private String login = "";
    @NotNull
    private String passwordHash = "";
    @NotNull
    private UserRoleEnum role = UserRoleEnum.USER;
}
