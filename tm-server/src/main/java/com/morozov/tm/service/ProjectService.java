package com.morozov.tm.service;

import com.morozov.tm.api.repository.IProjectRepository;
import com.morozov.tm.api.service.IProjectService;
import com.morozov.tm.dto.ProjectDto;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.exception.ProjectNotFoundException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.util.DataValidationUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public final class ProjectService implements IProjectService {
    @Autowired
    IProjectRepository projectRepository;

    @Override
    public final List<Project> findAllProject() {
        return projectRepository.findAll();
    }

    @Override
    public Project findOneById(@NotNull String projectId) throws ProjectNotFoundException {
        @NotNull Project project = projectRepository.findById(projectId).orElseThrow(ProjectNotFoundException::new);
        return project;

    }

    @Override
    public final List<Project> getAllProjectByUserId(@NotNull String userId) {
        return projectRepository.findByUser(userId);
    }

    @Override
    public final Project addProject(@NotNull final String projectName, @NotNull final User user)
            throws StringEmptyException {
        DataValidationUtil.checkEmptyString(projectName);
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setUser(user);
        projectRepository.save(project);
        return project;
    }

    @Override
    public final boolean removeProjectById(@NotNull final String userId, @NotNull String id)
            throws StringEmptyException, ProjectNotFoundException {
        boolean isDeleted;
        DataValidationUtil.checkEmptyString(id);
        @Nullable final Project removeProject = projectRepository.findByUserAndId(userId, id).orElseThrow(ProjectNotFoundException::new);
        projectRepository.delete(removeProject);
        isDeleted = true;
        return isDeleted;
    }

    @Override
    public final void updateProject(
            @NotNull final String id, @NotNull final String projectName, @NotNull final String projectDescription,
            @NotNull final String dataStart, @NotNull final String dataEnd, @NotNull final User user)
            throws ParseException, ProjectNotFoundException {
        @NotNull final Project project = projectRepository.findById(id).orElseThrow(ProjectNotFoundException::new);
        project.setId(id);
        project.setName(projectName);
        project.setDescription(projectDescription);
        final Date updatedStartDate = ConsoleHelperUtil.formattedData(dataStart);
        if (updatedStartDate != null) {
            project.setStatus(StatusEnum.PROGRESS);
        }
        project.setStartDate(updatedStartDate);
        final Date updatedEndDate = ConsoleHelperUtil.formattedData(dataEnd);
        if (updatedEndDate != null) {
            project.setStatus(StatusEnum.READY);
        }
        project.setEndDate(updatedEndDate);
        project.setUser(user);
        projectRepository.save(project);
    }

    @Override
    public final List<Project> searchProjectByString(@NotNull final String userId, @NotNull final String string) {
        @NotNull List<Project> projectListByUserId;
        if (string.isEmpty()) return projectRepository.findByUser(userId);
        projectListByUserId = projectRepository.searchProjectByString(userId, string);
        return projectListByUserId;
    }


    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        projectRepository.removeAllByUserId(userId);
    }

    @Override
    public final void clearProjectList() {
        projectRepository.deleteAll();
    }

    @Override
    public void loadProjectList(@Nullable List<Project> loadProjectList) {
       if(loadProjectList != null) projectRepository.saveAll(loadProjectList);
    }

    @NotNull
    @Override
    public ProjectDto transferProjectToProjectDto(@NotNull final Project project) {
        @NotNull final ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setName(project.getName());
        projectDto.setDescription(project.getDescription());
        projectDto.setUserId(project.getUser().getId());
        projectDto.setCreatedData(project.getCreatedData());
        projectDto.setStartDate(project.getStartDate());
        projectDto.setEndDate(project.getEndDate());
        projectDto.setStatus(project.getStatus());
        return projectDto;
    }

    @NotNull
    @Override
    public Project transferProjectDtoToProject(@NotNull final ProjectDto projectDto, @NotNull final User user) {
        @NotNull final Project project = new Project();
        project.setId(projectDto.getId());
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        project.setUser(user);
        project.setCreatedData(projectDto.getCreatedData());
        project.setStartDate(projectDto.getStartDate());
        project.setEndDate(projectDto.getEndDate());
        project.setStatus(projectDto.getStatus());
        return project;
    }

    @Override
    public List<ProjectDto> transferListProjectToListProjectDto(@NotNull List<Project> projectList) {
        @NotNull final List<ProjectDto> projectDtoList = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            projectDtoList.add(transferProjectToProjectDto(project));
        }
        return projectDtoList;
    }
}
