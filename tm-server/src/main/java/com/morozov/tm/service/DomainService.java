package com.morozov.tm.service;

import com.morozov.tm.api.service.IDomainService;
import com.morozov.tm.api.service.IProjectService;
import com.morozov.tm.api.service.ITaskService;
import com.morozov.tm.api.service.IUserService;
import com.morozov.tm.dto.DomainDto;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DomainService implements IDomainService {
    @Autowired
    private IProjectService projectService;
    @Autowired
    private IUserService userService;
    @Autowired
    private ITaskService taskService;

    @Override
    public void load(@NotNull final DomainDto domainDto) {
        projectService.loadProjectList(domainDto.getProjectList());
        taskService.loadTaskList(domainDto.getTaskList());
        userService.loadUserList(domainDto.getUserList());
    }

    @Override
    public void export(@NotNull final DomainDto domainDto) {
        domainDto.setProjectList(projectService.findAllProject());
        domainDto.setTaskList(taskService.findAllTask());
        domainDto.setUserList(userService.findAllUser());
    }
}
