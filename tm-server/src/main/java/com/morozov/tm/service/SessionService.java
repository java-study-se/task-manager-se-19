package com.morozov.tm.service;

import com.morozov.tm.api.repository.ISessionRepository;
import com.morozov.tm.api.service.ISessionService;
import com.morozov.tm.dto.SessionDto;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.AccessFirbidenException;
import com.morozov.tm.util.SessionUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class SessionService implements ISessionService {
    @Autowired
    private ISessionRepository sessionRepository;

    @Override
    public Session getNewSession(User user) {
        @NotNull final Session session = new Session();
        session.setUser(user);
        session.setTimestamp(new Date());
        Session sessionWithSign = SessionUtil.sign(session);
            sessionRepository.save(session);
        return sessionWithSign;
    }

    @Override
    public Session validate(@Nullable final Session session) throws AccessFirbidenException, CloneNotSupportedException {
        if (session == null) throw new AccessFirbidenException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessFirbidenException();
        if (session.getUser() == null) throw new AccessFirbidenException();
        if (session.getTimestamp() == null || !SessionUtil.isSessionTimeLive(session.getTimestamp()))
            throw new AccessFirbidenException();
        @NotNull final Session temp = session.clone();
        if (temp == null) throw new AccessFirbidenException();
        @NotNull final String signatureOrigin = session.getSignature();
        @Nullable final String signatureTemp = SessionUtil.sign(temp).getSignature();
        final boolean check = signatureOrigin.equals(signatureTemp);
        if (!check) throw new AccessFirbidenException();
            sessionRepository.findById(session.getId());
        return session;
    }

    @Override
    public void closeSession(@NotNull final Session session) {
            sessionRepository.delete(session);
    }

    @Override
    public SessionDto transferSessionToSessionDto(@NotNull final Session session, User user) {
        @NotNull final SessionDto sessionDto = new SessionDto();
        sessionDto.setId(session.getId());
        sessionDto.setSignature(session.getSignature());
        sessionDto.setTimestamp(session.getTimestamp());
        sessionDto.setUserId(session.getUser().getId());
        sessionDto.setUserRoleEnum(user.getRole());
        return sessionDto;
    }

    @Override
    public Session transferSessionDtoToSession(@NotNull SessionDto sessionDto, @NotNull User user) {
        @NotNull final Session session = new Session();
        session.setId(sessionDto.getId());
        session.setSignature(sessionDto.getSignature());
        session.setTimestamp(sessionDto.getTimestamp());
        session.setUser(user);
        return session;
    }
}
