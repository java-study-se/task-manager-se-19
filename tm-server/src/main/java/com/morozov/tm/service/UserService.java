package com.morozov.tm.service;

import com.morozov.tm.api.repository.IUserRepository;
import com.morozov.tm.api.service.IUserService;
import com.morozov.tm.dto.UserDto;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.*;
import com.morozov.tm.util.MD5HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public final class UserService implements IUserService {
    @Autowired
    private IUserRepository userRepository;

    @Override
    public User loginUser(@NotNull final String login, @NotNull final String password)
            throws UserNotFoundException {
        @Nullable User user = userRepository.findByLogin(login);
        final String typePasswordHash = MD5HashUtil.getHash(password);
        if (typePasswordHash == null) throw new UserNotFoundException();
        if (user == null) throw new UserNotFoundException();
        if (!typePasswordHash.equals(user.getPasswordHash())) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public String registryUser(@NotNull final String login, @NotNull final String password)
            throws UserExistException, StringEmptyException {
        @Nullable User oneByLogin = userRepository.findByLogin(login);
        if (oneByLogin != null) throw new UserExistException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setRole(UserRoleEnum.USER);
        final String hashPassword = MD5HashUtil.getHash(password);
        if (hashPassword == null) throw new StringEmptyException();
        user.setPasswordHash(hashPassword);
        userRepository.save(user);
        @NotNull final String userId = user.getId();
        return userId;
    }


    @Override
    public void updateUserPassword(@NotNull final String id, @NotNull final String newPassword)
            throws UserNotFoundException {
        @Nullable User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        String hashNewPassword = MD5HashUtil.getHash(newPassword);
        if (hashNewPassword != null) {
            user.setPasswordHash(hashNewPassword);
        }
        userRepository.save(user);
    }

    @Override
    public void updateUserProfile(@NotNull final String id, @NotNull final String newUserName, @NotNull final String newUserPassword)
            throws UserNotFoundException {
        @Nullable User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        user.setLogin(newUserName);
        String hashNewUserPassword = MD5HashUtil.getHash(newUserPassword);
        if (hashNewUserPassword != null) {
            user.setPasswordHash(hashNewUserPassword);
        }
        userRepository.save(user);
    }

    public void loadUserList(final List<User> userList) {
        if (userList != null) {
            for (User user : userList) {
                userRepository.save(user);
            }
        }
    }

    @Override
    public List<User> findAllUser() {
            return userRepository.findAll();
    }

    @Override
    public User findOneById(@NotNull String id) throws UserNotFoundException {
        @Nullable User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        return user;
    }

    @Override
    @NotNull
    public UserDto transferUserToUserDto(@NotNull final User user) {
        @NotNull final UserDto resultUserDto = new UserDto();
        resultUserDto.setId(user.getId());
        resultUserDto.setLogin(user.getLogin());
        resultUserDto.setPasswordHash(user.getPasswordHash());
        resultUserDto.setRole(user.getRole());
        return resultUserDto;
    }

    @Override
    @NotNull
    public List<UserDto> transferListUserToListUserDto(@NotNull final List<User> userList) {
        List<UserDto> resultList = new ArrayList<>();
        for (@NotNull final User user : userList) {
            resultList.add(transferUserToUserDto(user));
        }
        return resultList;
    }

}
