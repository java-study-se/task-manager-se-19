package com.morozov.tm.service;

import com.morozov.tm.api.repository.ITaskRepository;
import com.morozov.tm.api.service.ITaskService;
import com.morozov.tm.dto.TaskDto;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.Task;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.TaskNotFoundException;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.util.DataValidationUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public final class TaskService implements ITaskService {
    @Autowired
    private ITaskRepository taskRepository;

    @Override
    public List<Task> findAllTask() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> getAllTaskByUserId(@NotNull final String userId) {
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public Task addTask(final User user, @NotNull final String taskName, final Project project)
            throws StringEmptyException {
        DataValidationUtil.checkEmptyString(taskName);
        @NotNull final Task task;
        task = new Task();
        task.setName(taskName);
        task.setProject(project);
        task.setUser(user);
        taskRepository.save(task);
        return task;
    }

    @Override
    public boolean removeTaskById(@NotNull final String userId, @NotNull final String id)
            throws StringEmptyException, TaskNotFoundException {
        DataValidationUtil.checkEmptyString(id);
        boolean isDeleted;
        Task oneByUserId = taskRepository.findOneByUserId(userId, id).orElseThrow(TaskNotFoundException::new);
        taskRepository.delete(oneByUserId);
        isDeleted = true;
        return isDeleted;
    }

    @Override
    public void updateTask(
            final User user, @NotNull final String id, @NotNull final String name, @NotNull final String description,
            @NotNull final String dataStart, @NotNull final String dataEnd, @NotNull final Project project)
            throws StringEmptyException, ParseException, TaskNotFoundException {
        DataValidationUtil.checkEmptyString(id, name, description);
        final Task task = taskRepository.findById(id).orElseThrow(TaskNotFoundException::new);
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        final Date updatedStartDate = ConsoleHelperUtil.formattedData(dataStart);
        if (updatedStartDate != null) {
            task.setStatus(StatusEnum.PROGRESS);
        }
        task.setStartDate(updatedStartDate);
        final Date updatedEndDate = ConsoleHelperUtil.formattedData(dataEnd);
        if (updatedEndDate != null) {
            task.setStatus(StatusEnum.READY);
        }
        task.setEndDate(updatedEndDate);
        task.setProject(project);
        task.setUser(user);
        taskRepository.save(task);
    }

    @Override
    public List<Task> getAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId)
            throws StringEmptyException {
        DataValidationUtil.checkEmptyString(projectId);
        return taskRepository.findAllByProjectIdUserId(userId, projectId);
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        taskRepository.deleteAllTaskByProjectId(userId, projectId);
    }

    @Override
    public void clearTaskList() {
        taskRepository.deleteAll();
    }

    @Override
    public List<Task> searchTaskByString(@NotNull final String userId, @NotNull final String string) {
        if (string.isEmpty()) return taskRepository.findAllByUserId(userId);
        return taskRepository.searchTaskByString(userId, string);
    }

    @Override
    public void removeAllTaskByUserId(@NotNull final String userId) {
        taskRepository.removeAllByUserId(userId);
    }

    @Override
    public void loadTaskList(@Nullable final List<Task> loadList) {
        if (loadList != null) {
            for (Task task : loadList) {
                taskRepository.save(task);
            }
        }
    }

    @Override
    public @NotNull TaskDto transferTaskToTaskDto(@NotNull Task task) {
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setId(task.getId());
        taskDto.setName(task.getName());
        taskDto.setDescription(task.getDescription());
        taskDto.setCreatedData(task.getCreatedData());
        taskDto.setStartDate(task.getStartDate());
        taskDto.setEndDate(task.getEndDate());
        taskDto.setStatus(task.getStatus());
        if (task.getUser() != null) taskDto.setUserId(task.getUser().getId());
        if (task.getProject() != null) taskDto.setIdProject(task.getProject().getId());
        return taskDto;
    }

    @Override
    public Task transferTaskDtoToTask(@NotNull TaskDto taskDto, @NotNull final User user, @NotNull final Project project) {
        @NotNull final Task task = new Task();
        task.setId(taskDto.getId());
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setCreatedData(taskDto.getCreatedData());
        task.setStartDate(taskDto.getStartDate());
        task.setEndDate(taskDto.getEndDate());
        task.setStatus(taskDto.getStatus());
        task.setUser(user);
        task.setProject(project);
        return task;
    }

    @Override
    public @NotNull List<TaskDto> transferListTaskToListTaskDto(@NotNull List<Task> taskList) {
        List<TaskDto> taskDtoList = new ArrayList<>();
        for (@NotNull final Task task : taskList) {
            taskDtoList.add(transferTaskToTaskDto(task));
        }
        return taskDtoList;
    }
}

