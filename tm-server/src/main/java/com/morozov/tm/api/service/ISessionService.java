package com.morozov.tm.api.service;

import com.morozov.tm.dto.SessionDto;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.AccessFirbidenException;
import org.jetbrains.annotations.NotNull;


public interface ISessionService {

    Session getNewSession(final User user);

    Session validate(final Session session) throws AccessFirbidenException, CloneNotSupportedException;

    void closeSession(@NotNull final Session session);

    SessionDto transferSessionToSessionDto(@NotNull final Session session, User user);

    Session transferSessionDtoToSession(@NotNull final SessionDto sessionDto, @NotNull final User user);
}
