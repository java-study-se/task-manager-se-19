package com.morozov.tm.api.service;


import com.morozov.tm.dto.ProjectDto;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;


import java.text.ParseException;
import java.util.List;

public interface IProjectService {
    List<Project> findAllProject();

    Project findOneById(@NotNull final String projectId) throws ProjectNotFoundException;

    List<Project> getAllProjectByUserId(@NotNull String userId);

    Project addProject(@NotNull String projectName, User user) throws StringEmptyException;

    boolean removeProjectById(@NotNull String userId, @NotNull String id) throws StringEmptyException, ProjectNotFoundException;

    void updateProject(
            @NotNull String id, @NotNull String projectName, @NotNull String projectDescription,
            @NotNull String dataStart, @NotNull String dataEnd, @NotNull User user)
            throws ParseException, ProjectNotFoundException;

    List<Project> searchProjectByString(@NotNull String userId, @NotNull String string);


    void removeAllByUserId(@NotNull String userId);

    void clearProjectList();

    void loadProjectList(@Nullable final List<Project> loadProjectList);

    @NotNull
    ProjectDto transferProjectToProjectDto(@NotNull final Project project);

    @NotNull
    Project transferProjectDtoToProject(@NotNull final ProjectDto projectDto, @NotNull final User user);

    List<ProjectDto> transferListProjectToListProjectDto(@NotNull final List<Project> projectList);
}
