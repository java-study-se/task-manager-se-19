package com.morozov.tm.api.service;


import com.morozov.tm.dto.TaskDto;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.Task;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;


import java.text.ParseException;
import java.util.List;

public interface ITaskService {
    List<Task> findAllTask();

    List<Task> getAllTaskByUserId(@NotNull String userId);

    Task addTask(User user, @NotNull String taskName, Project project)
            throws StringEmptyException;

    boolean removeTaskById(@NotNull String userId, @NotNull String id) throws StringEmptyException, TaskNotFoundException;

    void updateTask(User user, @NotNull String id, @NotNull String name, @NotNull String description,
                    @NotNull String dataStart, @NotNull String dataEnd, Project project)
            throws StringEmptyException, ParseException, TaskNotFoundException;

    List<Task> getAllTaskByProjectId(@NotNull String userId, @NotNull String projectId)
            throws StringEmptyException;

    void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId);

    void clearTaskList();

    List<Task> searchTaskByString(@NotNull String userId, @NotNull String string);

    void removeAllTaskByUserId(@NotNull String userId);

    void loadTaskList(@Nullable final List<Task> loadList);

    @NotNull
    TaskDto transferTaskToTaskDto(@NotNull final Task task);

    Task transferTaskDtoToTask(@NotNull final TaskDto taskDto, @NotNull final User user, @NotNull final Project project);

    @NotNull
    List<TaskDto> transferListTaskToListTaskDto(@NotNull final List<Task> taskList);

}
