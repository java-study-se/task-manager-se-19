-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.23 - MySQL Community Server (GPL)
-- Операционная система:         Win64
-- HeidiSQL Версия:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных tm_morozov
CREATE DATABASE IF NOT EXISTS `tm_morozov` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tm_morozov`;

-- Дамп структуры для таблица tm_morozov.app_project
CREATE TABLE IF NOT EXISTS `app_project` (
  `id` varchar(255) NOT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) NOT NULL,  
  PRIMARY KEY (`id`),
  KEY `FKp9byv3k2r7rgg7svn3rx10a1u` (`user_id`),
  CONSTRAINT `FKp9byv3k2r7rgg7svn3rx10a1u` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица tm_morozov.app_session
CREATE TABLE IF NOT EXISTS `app_session` (
  `id` varchar(255) NOT NULL,
  `signature` varchar(255) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `user_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrrdhu5ryvprfqplat774p2n4t` (`user_id`),
  CONSTRAINT `FKrrdhu5ryvprfqplat774p2n4t` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица tm_morozov.app_task
CREATE TABLE IF NOT EXISTS `app_task` (
  `id` varchar(255) NOT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) NOT NULL,
  `project_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkc5pwubxw7j4b0xprgdmgkrel` (`user_id`),
  KEY `FKsu3pcsyuwrs6nmpcpufikq5u4` (`project_id`),
  CONSTRAINT `FKkc5pwubxw7j4b0xprgdmgkrel` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`),
  CONSTRAINT `FKsu3pcsyuwrs6nmpcpufikq5u4` FOREIGN KEY (`project_id`) REFERENCES `app_project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица tm_morozov.app_user
CREATE TABLE IF NOT EXISTS `app_user` (
  `id` varchar(255) NOT NULL,
  `login` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_irayhia1ygarvmv7apksctnqn` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `app_user` (`id`, `login`, `password_hash`, `role`) VALUES
	('7c0a6cae-74d3-4ac9-bef5-d046fa4efe09', 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'USER'),
	('e847fedf-2d4a-4735-a48c-544930f03552', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN');

-- Экспортируемые данные не выделены.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
